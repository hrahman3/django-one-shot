from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList


# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo,
    }

    return redirect("todo_list_list")
